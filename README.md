
# OOP - Object Oriented Programming Assignment - Multi-Car Parking



Our task was to build and organize a prototype of a Multi Car Parking, where there would be
division between two users of the app - Driver and Employee. The system was designed to
introduce us into thinking from both perspective – one is for the user, the person who wants to
park and later on collect their car (either with the assistance of the employee or not). Second
perspective was the employee of Multi Car Parking prototype where the system enables us to
assign employees to their task with assistance of parking cars but also adding and managing
employees We also had to have in mind about making different prices for fares depending on
what vehicle the user drives in and if the driver is disabled or not.
With all that in mind there were lots of requirements that were needed to be done That might
be really tricky at the start but with the proper preparation and organization it will be much
easier. I started with reading the assignment requirements and creating pseudo class diagram.
Later on, I needed to organize class ParkingApplication so that it would be clear and
understandable to our users.




